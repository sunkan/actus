<?php
/**
 * Actus Path
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package   Actus
 * @license   MIT
 * @link      https://bitbucket.org/sunkan/actus
 * @author    Sergey Kalistratov <kalistratov.s.m@gmail.com>
 * @author    Andreas Sundqvist <andreas@forme.se>
 */

namespace Actus;

/**
 * Class Exception
 * @package Actus
 */
class Exception extends \Exception
{

}
