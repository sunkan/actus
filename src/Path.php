<?php
/**
 * Actus Path
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * Adapted to make it easier to configure with di
 *
 * @package   Actus
 * @license   MIT
 * @link      https://github.com/sunkan/actus
 * @author    Sergey Kalistratov <kalistratov.s.m@gmail.com>
 * @author    Andreas Sundqvist <andreas@forme.se>
 */

namespace Actus;

/**
 * Class Path
 * @package Actus
 */
class Path
{
    /**
     * Minimal alias name length.
     *
     * @var string
     */
    const MIN_ALIAS_LENGTH = 2;

    /**
     * Mod prepend rule add paths.
     *
     * @var string
     */
    const MOD_PREPEND = 'prepend';

    /**
     * Mod append rule add paths.
     *
     * @var string
     */
    const MOD_APPEND = 'append';

    /**
     * Reset all registered paths.
     *
     * @var string
     */
    const MOD_RESET = 'reset';

    /**
     * Holds paths list.
     *
     * @var array
     */
    protected $_paths = array();

    /**
     * Root dir.
     *
     * @var string
     */
    protected $_root;

    /**
     * Root domain.
     *
     * @var string
     */
    protected $_rootDomain;

    /**
     * Root domain for alias.
     *
     * @var array
     */
    protected $_rootDomains = [];

    /**
     * Make it easy to setup with di configuration
     *
     * @param array $aliases
     */
    public function setAliases(array $aliases)
    {
        foreach ($aliases as $alias => $paths) {
            $this->set($alias, $paths, Path::MOD_RESET);
        }
    }

    /**
     * Register alias locations in file system.
     * Example:
     *      "default:file.txt" - if added at least one path and
     *      "C:\server\test.dev\fy-folder" or "C:\server\test.dev\fy-folder\..\..\"
     *
     * @param string       $alias
     * @param string|array $paths
     * @param string  $mode
     *
     * @throws Exception
     */
    public function set(string $alias, $paths, string $mode = Path::MOD_PREPEND)
    {
        $paths = (array) $paths;
        $alias = $this->_cleanAlias($alias);

        if (strlen($alias) < Path::MIN_ALIAS_LENGTH) {
            throw new Exception(sprintf('The minimum number of characters is %s', Path::MIN_ALIAS_LENGTH));
        }

        if ($mode === self::MOD_RESET) {
            // Reset mode
            $this->_paths[$alias] = array();

            $mode = self::MOD_PREPEND; // Add new paths in Prepend mode
        }

        foreach ($paths as $path) {
            if (!isset($this->_paths[$alias])) {
                $this->_paths[$alias] = array();
            }

            $path = $this->_clean($path);
            if ($path && !in_array($path, $this->_paths[$alias], true)) {
                if (preg_match('/^' . preg_quote($alias . ':') . '/i', $path)) {
                    throw new Exception(sprintf('Added looped path "%s" to key "%s"', $path, $alias));
                }

                $this->_addNewPath($path, $alias, $mode);
            }
        }
    }

    /**
     * Old version of set() method
     *
     * @param string|array $paths
     * @param string       $alias
     * @param string       $mode
     * @throws Exception
     *
     * @deprecated
     */
    public function add($paths, string $alias = 'default', string $mode = Path::MOD_PREPEND)
    {
        $this->set($alias, $paths, $mode);
    }

    /**
     * Normalize and clean path.
     *
     * @param string $path ("C:\server\test.dev\file.txt")
     * @return string
     */
    public function clean(string $path): string
    {
        $tokens = array();
        $path = $this->_clean($path);
        $prefix = $this->prefix($path);
        $path = substr($path, strlen($prefix));
        $parts = array_filter(explode('/', $path), 'strlen');

        foreach ($parts as $part) {
            if ('..' === $part) {
                array_pop($tokens);
            } elseif ('.' !== $part) {
                array_push($tokens, $part);
            }
        }

        return $prefix . implode('/', $tokens);
    }

    /**
     * Get absolute path to a file or a directory.
     *
     * @param $source (example: "default:file.txt")
     * @param bool $local if file/folder is on local drive
     * @return null|string
     */
    public function get(string $source, bool $local = true): ?string
    {
        list(, $paths, $path) = $this->_parse($source, $local);
        return $this->_find($paths, $path, false, $local);
    }

    /**
     * Get absolute path to a file or a directory.
     *
     * @param $source (example: "default:file.txt")
     * @return array
     */
    public function glob(string $source): array
    {
        list(, $paths, $path) = $this->_parse($source);
        return $this->_find($paths, $path, true);
    }

    /**
     * Get all absolute path to a file or a directory.
     *
     * @param $source (example: "default:file.txt")
     * @return mixed
     */
    public function getPaths(string $source): array
    {
        $source = $this->_cleanSource($source);
        list(, $paths) = $this->_parse($source);

        return $paths;
    }

    /**
     * Get root directory.
     *
     * @return string
     * @throws Exception
     */
    public function getRoot(): string
    {
        $this->_checkRoot();
        return $this->_root;
    }

    /**
     * Check virtual or real path.
     *
     * @param string $path (example: "default:file.txt" or "C:\server\test.dev\file.txt")
     * @return bool
     */
    public function isVirtual(string $path): bool
    {
        $parts = explode(':', $path, 2);

        list($alias) = $parts;
        $alias = $this->_cleanAlias($alias);
        if ($this->prefix($path) !== null && !array_key_exists($alias, $this->_paths)) {
            return false;
        }

        return (count($parts) == 2) ? true : false;
    }

    /**
     * Get path prefix.
     *
     * @param string $path (example: "C:\server\test.dev\file.txt")
     * @return null
     */
    public function prefix(string $path): ?string
    {
        $path = $this->_clean($path);
        $rs = preg_match('|^(?P<prefix>([a-zA-Z]+:)?//?)|', $path, $matches);
        return $rs ? $matches['prefix'] : null;
    }

    /**
     * Remove path from registered paths for source
     *
     * @param string       $fromSource (example: "default:file.txt")
     * @param string|array $paths
     * @return bool
     */
    public function remove(string $fromSource, $paths): bool
    {
        $paths = (array) $paths;
        $fromSource = $this->_cleanSource($fromSource);
        list($alias) = $this->_parse($fromSource);

        $return = false;

        foreach ($paths as $origPath) {
            $path = $this->_cleanPath($this->_clean($origPath));

            $key = array_search($path, $this->_paths[$alias], true);
            if (false !== $key) {
                unset($this->_paths[$alias][$key]);
                $return = true;
            }
        }

        return $return;
    }

    /**
     * Setup root directory.
     *
     * @param string $dir
     * @throws Exception
     */
    public function setRoot(string $dir)
    {
        if (!is_dir($dir)) {
            throw new Exception(sprintf('Not found directory: %s', $dir));
        }

        $this->_root = $this->_clean($dir);
    }

    public function setRootDomain(string $alias, ?string $domain = null)
    {
        if ($domain) {
            $this->_rootDomains[$alias] = $domain;
        } else {
            $this->_rootDomain = $alias;
        }
    }

    public function setRootDomains(array $domains)
    {
        foreach ($domains as $alias => $domain) {
            $alias = $this->_cleanAlias($alias);
            $this->_rootDomains[$alias] = $domain;
        }
    }

    /**
     * Get url to a file.
     *
     * @param string    $source (example: "default:file.txt" or "C:\server\test.dev\file.txt")
     * @param bool      $full
     * @return null|string
     */
    public function url(string $source, bool $full = true): ?string
    {
        $details = explode('?', $source);

        $path = $details[0];
        $path = $this->_cleanPath($path);
        $path = $this->_getUrlPath($path, false);
        if (!empty($path)) {
            if (isset($details[1])) {
                $path .= '?' . $details[1];
            }

            $path = '/' . $path;
            if ($full) {
                $alias = false;
                if ($this->isVirtual($details[0])) {
                    list($alias) = explode(':', $details[0]);
                }
                return $this->_urlRoot($alias) . $path;
            }
            return $path;
        }

        return null;
    }

    /**
     * Get relative path to file or directory
     *
     * @param string $source (example: "default:file.txt")
     * @return null|string
     */
    public function rel(string $source): ?string
    {
        $fullpath = (string) $this->get($source);
        return $this->_getRelative($fullpath, $this->_root, '/');
    }

    /**
     * Get list of relative path to file or directory
     *
     * @param string $source (example: "default:*.txt")
     * @return array
     */
    public function relGlob(string $source): array
    {
        $list = (array) $this->glob($source);
        foreach ($list as $key => $item) {
            $list[$key] = $this->_getRelative($item, $this->_root, '/');
        }

        return $list;
    }

    /**
     * Add path to hold.
     *
     * @param string|array $path (example: "default:file.txt" or "C:/Server/public_html/index.php")
     * @param string       $alias
     * @param string|bool  $mode
     * @return void
     */
    protected function _addNewPath($path, string $alias, string $mode)
    {
        if ($cleanPath = $this->_cleanPath($path)) {
            if ($mode == self::MOD_PREPEND) {
                array_unshift($this->_paths[$alias], $cleanPath);
            }

            if ($mode == self::MOD_APPEND) {
                array_push($this->_paths[$alias], $cleanPath);
            }
        }
    }

    /**
     * Check root directory.
     *
     * @throws Exception
     */
    protected function _checkRoot()
    {
        if ($this->_root === null) {
            throw new Exception(sprintf('Please, set the root directory'));
        }
    }

    /**
     * Find actual file or directory in the paths.
     *
     * @param string|array $paths
     * @param string       $file
     * @param bool         $isGlob
     * @return null|string|array
     */
    protected function _find($paths, string $file, bool $isGlob = false, bool $isLocal = true)
    {
        $paths = (array) $paths;
        $file = ltrim($file, "\\/");

        foreach ($paths as $path) {
            $fullPath = $this->clean($path . '/' . $file);

            if ($isGlob) {
                $paths = glob($fullPath);
                $paths = array_filter((array) $paths);
                return $paths ?: array();
            } elseif ($isLocal === false) {
                return $fullPath;
            } elseif (file_exists($fullPath) || is_dir($fullPath)) {
                return $fullPath;
            }
        }

        return null;
    }

    /**
     * Get add path.
     *
     * @param string $path (example: "default:file.txt" or "C:/Server/public_html/index.php")
     * @param string $path
     * @return null|string
     */
    protected function _cleanPath(string $path): ?string
    {
        if ($this->isVirtual($path)) {
            return $this->_clean($path);
        }

        if ($this->_hasCDBack($path)) {
            $realpath = $this->_clean(realpath($path));
            return $realpath ?: null;
        }

        return $this->_clean($path);
    }

    /**
     * Get url path.
     *
     * @param string $path (example: "default:file.txt" or "C:/Server/public_html/index.php")
     * @param bool   $exitsFile
     * @return string
     * @throws Exception
     */
    protected function _getUrlPath(string $path, bool $exitsFile = false): string
    {
        $this->_checkRoot();

        $path = $this->_cleanPath($path);
        if ($this->isVirtual($path)) {
            $path = $this->get($path, false);
        }
        $subject = $path;
        $pattern = '/^' . preg_quote($this->_root, '/') . '/i';

        if ($exitsFile && !$this->isVirtual($path) && !file_exists($path)) {
            $subject = null;
        }
        return ltrim(preg_replace($pattern, '', $subject), '/');
    }

    /**
     * Check has back current.
     *
     * @param $path
     * @return int
     */
    protected function _hasCDBack(string $path): int
    {
        $path = $this->_clean($path);
        return preg_match('(/\.\.$|/\.\./$)', $path);
    }

    /**
     * Parse source string.
     *
     * @param string $source (example: "default:file.txt")
     * @return array
     */
    protected function _parse(string $source, bool $local = true): array
    {
        $path = null;
        list($alias, $path) = explode(':', $source, 2);

        $path = ltrim($path, "\\/");

        $paths = $this->_resolvePaths($alias, $local);
        return array($alias, $paths, $path);
    }

    /**
     * @param string $alias
     * @return array
     */
    protected function _resolvePaths(string $alias, bool $local = true): array
    {
        $alias = $this->_cleanAlias($alias);

        $paths = isset($this->_paths[$alias]) ? $this->_paths[$alias] : array();
        $result = array();
        foreach ($paths as $originalPath) {
            if ($this->isVirtual($originalPath)) {
                if ($realPath = $this->get($originalPath)) {
                    $path = $realPath;
                } else {
                    $path = $this->_cleanPath($originalPath);
                }
            } else {
                $path = $this->_cleanPath($originalPath);
            }

            $result[] = $local ? realpath($path) : $path;
        }

        $result = array_filter($result); // remove empty
        $result = array_values($result); // reset keys

        return $result;
    }

    /**
     * @param $alias
     * @return mixed|string
     */
    protected function _cleanAlias(string $alias): string
    {
        $alias = preg_replace('/[^a-z0-9_\.-]/i', '', $alias);
        return $alias;
    }

    /**
     * @param string $source
     * @return mixed|string
     */
    protected function _cleanSource(string $source): string
    {
        $source = $this->_cleanAlias($source);
        $source .= ':';

        return $source;
    }

    /**
     * Forced clean path with linux-like sleshes
     *
     * @param string $path
     * @return string
     */
    protected function _clean(string $path): string
    {
        if (empty($path)) {
            return '';
        }
        $path = trim((string) $path);
        if (empty($path)) {
            $path = $_SERVER['DOCUMENT_ROOT'] ?: '';
        } else {
            $path = preg_replace('#[/\\\\]+#', '/', $path);
        }
        return $path;
    }

    protected function _getRelative(string $filePath, ?string $rootPath = null, string $forceDS = DIRECTORY_SEPARATOR): string
    {
        // Cleanup file path
        if (!$this->_isReal($filePath)) {
            $filePath = realpath($filePath);
        }
        $filePath = $this->_clean($filePath, $forceDS);

        if (!$this->_isReal($rootPath)) {
            $rootPath = realpath($rootPath);
        }
        $rootPath = $this->_clean($rootPath, $forceDS);
        // Remove root part
        $relPath = preg_replace('#^' . preg_quote($rootPath) . '#i', '', $filePath);
        $relPath = ltrim($relPath, $forceDS);
        return $relPath;
    }

    protected function _isReal(string $path): bool
    {
        $expected = $this->clean(realpath($path));
        $actual = $this->clean($path);
        return $expected === $actual;
    }

    protected function _urlRoot($alias = false): string
    {
        $host = $this->_rootDomain ?: $_SERVER['HTTP_HOST'];
        if ($alias && isset($this->_rootDomains[$alias])) {
            $host = $this->_rootDomains[$alias];
        }

        return '//' . $host;
    }
}
