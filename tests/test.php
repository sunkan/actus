<?php

include '../vendor/autoload.php';
$_SERVER['HTTP_HOST'] = 'error';
//  Get needed classes.
use Actus\Path;

//  Get path instance.
$path = new Path();

//  Setup root directory.
$path->setRoot(__DIR__);

$path->setRootDomains([
    'css' => 'static.stylewish.me',
    'collage' => 'files.stylewish.me',
]);

// Set multiple aliases
$path->setAliases([
    'collage' => ['collage'],
    'less' => [
        __DIR__ . '/styles/folder/less',
        __DIR__ . '/theme/styles/less',
    ],
    'css' => [
        __DIR__ . '/styles/folder/css',
        __DIR__ . '/theme/styles/css',
    ],
]);

//  Add paths.
$path->add(__DIR__ . '/styles/css', 'css', Path::MOD_APPEND);

//  Get added path list by key.
#var_dump($path->getPaths('css:'));
#var_dump($path->getPaths('less:'));

/**
 * Get full path for the first file found, if file exits.
 */
echo 'P1-error: ' . $path->get('css:styles_no.css', false) . "\n"; //  result: C:/Server/jbzoo/styles/css/styles.css
echo 'P1: ' . $path->get('css:styles.css') . "\n"; //  result: C:/Server/jbzoo/styles/css/styles.css
echo 'P2: ' . $path->get('css:test.css') . "\n"; //  result: C:/Server/jbzoo/styles/css/styles.css
echo 'P2-rel: ' . $path->rel('css:test.css') . "\n"; //  result: C:/Server/jbzoo/styles/css/styles.css
#echo "P2: " . $path->get('less:sub/styles.less') . "\n"; //  result: C:/Server/jbzoo/styles/folder/less/path/to/styles.less

/**
 * Get url for the first file found, if file exits.
 * If - "C:/Server/jbzoo" is root dir we have...
 */
var_dump($path->url('css:styles.css')); //  http://my-site.com/styles/css/styles.css
var_dump($path->url('css:styles-no.css')); //  http://my-site.com/styles/css/styles.css
var_dump($path->url('less:sub/styles.less')); //  http://my-site.com/styles/css/folder/less/path/to/styles.less
var_dump($path->url('collage:100/1.jpg')); //  http://my-site.com/styles/css/folder/less/path/to/styles.less

#echo '<link rel="stylesheet" href="' . $path->url('css:styles.css') . '">';
