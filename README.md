# JBZoo Path

This is a fork of https://github.com/JBZoo/Path. I needed it to be easy to configure with DI

#### Virtual file system

[![License](https://poser.pugx.org/sunkan/actus/license)](https://packagist.org/packages/sunkan/actus)
[![Latest Stable Version](https://poser.pugx.org/sunkan/actus/v/stable)](https://packagist.org/packages/sunkan/actus)

### How to use

```php
require_once './vendor/autoload.php'; // composer autoload.php

//  Get needed classes.
use Actus\Path;

//  Get path instance.
$path = new Path();

//  Setup root directory.
$path->setRoot(__DIR__);

// Set multiple aliases
$path->setAliases([
  'less' => [
    __DIR__ . 'styles/folder/less',
    __DIR__ . 'theme/styles/less',
  ],
  'css' => [
    __DIR__ . 'styles/folder/css',
    __DIR__ . 'theme/styles/css',
  ]
]);

//  Add paths.
$path->add(__DIR__ . '/styles/css', 'css');
$path->add(__DIR__ . '/simple/styles/css', 'css');

//  Add array paths.
$path->add(array(
    __DIR__ . 'styles/folder/less',
    __DIR__ . 'theme/styles/less',
), 'less');

/**
 * Add paths by virtual.
 * If you already added at least one one way, you can use the virtual paths
 */
$path->add('less:assets/less');
$path->add('css:assets/less');

//  Get added path list by key.
var_dump($path->getPaths('css:'));
var_dump($path->getPaths('less:'));

/**
 * Get full path for the first file found, if file exits.
 */
echo $path->get('css:styles.css');           //  result: C:/Server/jbzoo/styles/css/styles.css
echo $path->get('less:path/to/styles.less'); //  result: C:/Server/jbzoo/styles/folder/less/path/to/styles.less

/**
 * Get url for the first file found, if file exits.
 * If - "C:/Server/jbzoo" is root dir we have...
 */
$path->url('css:styles.css');           //  http://my-site.com/styles/css/styles.css
$path->url('less:path/to/styles.less')  //  http://my-site.com/styles/css/folder/less/path/to/styles.less

echo '<link rel="stylesheet" href="' . $path->url('css:styles.css') . '">';

/**
 * Need remove added path?
 * Second parameter is the key of the paths array.
 */
$path->remove('less:', array(1));

//  or use string
$path->remove('less:', 1);

//  Clean path.
$path->clean('C:\server/folder\\\file.txt'); // result: 'C:/server/folder/file.txt'
$path->clean('path\\to//simple\\folder')    //  result: 'path/to/simple/folder'
```
